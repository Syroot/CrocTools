# CrocTools

This repository provides a .NET library (written in C#) which is capable of loading, modifying and saving game data of
the 1997 3D platformer "Croc: Legend of the Gobbos", created by Argonaut Software.

## Available Tools

- **ContainerPacker** is a command line tool for packing or unpacking game data containers (IDX / WAD file pairs) and decompress their contents.
- **io_mesh_mod** is a currently very limited Blender add-on loading the Croc MOD model format.