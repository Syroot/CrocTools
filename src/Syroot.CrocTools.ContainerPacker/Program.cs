﻿using System;
using System.Collections.Generic;
using System.IO;
using Syroot.CrocTools.Containers;

namespace Syroot.CrocTools.Test
{
    /// <summary>
    /// Represents the main class of the application containing the program entry point.
    /// </summary>
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main(string[] args)
        {
            // Check if either a file or directory has been passed.
            string path = args.Length == 1 ? args[0] : null;
            if (String.IsNullOrEmpty(path))
            {
                WriteUsage();
            }
            else if (Directory.Exists(path))
            {
                PackContainer(path);
            }
            else if (File.Exists(Path.ChangeExtension(path, Container.IndexFileExtension))
                && (File.Exists(Path.ChangeExtension(path, Container.DataFileExtension))))
            {
                UnpackContainer(path);
            }
            else
            {
                Console.WriteLine($"File or directory \"{path}\" does not exist.");
            }
#if DEBUG
            Console.ReadLine();
#endif
        }

        private static void WriteUsage()
        {
            Console.WriteLine("Packs or unpacks Croc data container contents and decompresses them.");
            Console.WriteLine();
            Console.WriteLine("CONTAINERPACKER FILE|FOLDER");
            Console.WriteLine();
            Console.WriteLine("        FILE   	The name of a data container or index file to unpack");
            Console.WriteLine("                 into a directory of the same name.");
            Console.WriteLine("        FOLDER   The name of a directory which contents will be stored");
            Console.WriteLine("                 in index and data container files of the same name.");
        }

        private static void PackContainer(string directoryName)
        {
            Console.WriteLine($"Packing \"{Path.GetFileName(directoryName)}\"...");

            // Create the container and pack all files into it.
            Container container = new Container();
            container.PackAll(directoryName);

            // Save the container.
            Console.Write($"Saving container... ");
            container.Save(directoryName);
            Console.WriteLine("OK");
        }

        private static void UnpackContainer(string fileName)
        {
            string outputDirectory = Path.ChangeExtension(fileName, null);
            Console.WriteLine($"Unpacking \"{Path.GetFileName(outputDirectory)}\"...");

            // Load the container.
            Container container = new Container(fileName);
            // Create the directory and unpack all files into it.
            Directory.CreateDirectory(outputDirectory);
            container.UnpackAll(outputDirectory);

            Console.WriteLine("Done.");
        }
    }
}