﻿using System.Windows;
using Caliburn.Micro;
using Syroot.CrocTools.MapEditor.ViewModels;

namespace Syroot.CrocTools.MapEditor
{
    public class Bootstrapper : BootstrapperBase
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Bootstrapper()
        {
            Initialize();
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainViewModel>();
        }
    }
}
