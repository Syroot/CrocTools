﻿using System.Windows;
using Caliburn.Micro;
using Syroot.CrocTools.Maps;

namespace Syroot.CrocTools.MapEditor.ViewModels
{
    public class MainViewModel : PropertyChangedBase
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private string _windowTitle;
        private string _fileName;
        private Map _map;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string FileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
                NotifyOfPropertyChange(() => _fileName);
                NotifyOfPropertyChange(() => _windowTitle);
                NotifyOfPropertyChange(() => CanSaveMap);
                NotifyOfPropertyChange(() => CanSaveMapAs);
                NotifyOfPropertyChange(() => CanCloseMap);
            }
        }

        public Map Map
        {
            get { return _map; }
            set
            {
                _map = value;
                NotifyOfPropertyChange(() => _map);
                NotifyOfPropertyChange(() => CanSaveMap);
                NotifyOfPropertyChange(() => CanSaveMapAs);
                NotifyOfPropertyChange(() => CanCloseMap);
            }
        }

        public string WindowTitle
        {
            get { return _windowTitle; }
            set
            {
                _windowTitle = value;
                NotifyOfPropertyChange(() => _windowTitle);
            }
        }

        public bool CanSaveMap
        {
            get { return FileName != null; }
        }

        public bool CanSaveMapAs
        {
            get { return Map != null; }
        }

        public bool CanCloseMap
        {
            get { return Map != null; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void NewMap()
        {
            FileName = null;
            Map = new Map();
        }

        public void OpenMap()
        {

        }

        public void SaveMap()
        {

        }

        public void SaveMapAs()
        {

        }

        public void CloseMap()
        {
            FileName = null;
            Map = null;
        }

        public void Exit()
        {
            Application.Current.Shutdown();
        }
    }
}
