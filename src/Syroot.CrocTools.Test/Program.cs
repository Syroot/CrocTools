﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ImageSharp;
using Syroot.CrocTools.Containers;
using Syroot.CrocTools.Materials;
using Syroot.CrocTools.Pictures;
using Syroot.Maths;

namespace Syroot.CrocTools.Test
{
    internal class Program
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private const string _dataPath = @"C:\Games\Croc\gdata";
        private const string _extractPath = @"D:\Pictures\CrocExtract";

        private static readonly Stopwatch _stopwatch = new Stopwatch();
        private static readonly List<string> _exclusionList = new List<string>
        {
            // Models without material names, check Face structure and model flags.
            "BEBABLUE.MOD", "DANTTOP.MOD", "DT12.MOD", "NULL.MOD", "OBJECT01.MOD", "OBJECT05.MOD", "PENGSPR1.MOD",
            "PENGSPR2.MOD", "PENGSPR3.MOD", "PENGSPR4.MOD", "PENGSPR5.MOD", "PENGSPR6.MOD", "PENGSPR7.MOD",
            "PENGSPR8.MOD", "SECRETRO.MOD", "SMBKBLUE.MOD", "SMBKGREN.MOD", "SMBKRED.MOD", "SMBKYELW.MOD",
            "SNAPPY.MOD",
            "SWIMCROC.MOD" // Not even mesh count at start is right
        };

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main(string[] args)
        {
            LoadFiles("*.mat", (s) => new MaterialList(s));

            //Directory.CreateDirectory(_extractPath);
            //LoadData("*.pix", (s) => ExtractPix(new PixFile(s), s));
            //LoadData("*.wad", ExtractPackedPix);

            //LoadData("*.mod", (s) => new ModelFile(s));

            Console.WriteLine("Done.");
            Console.ReadLine();
        }
        
        private static void LoadFiles(string extension, Action<Stream> callback)
        {
            extension = extension.TrimStart(new[] { '*', '.' }).ToUpper();

            // Load raw files first.
            Console.WriteLine("Loading raw files...");
            foreach (string fileName in Directory.GetFiles(_dataPath, "*." + extension, SearchOption.AllDirectories))
            {
                if (_exclusionList.Contains(Path.GetFileName(fileName).ToUpper())) continue;

                Console.Write($"\tLoading {fileName}...");
                using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    LoadFile(stream, callback);
                }
            }

            // Load packed files.
            Console.WriteLine("Loading packed files...");
            foreach (string fileName in Directory.GetFiles(_dataPath, "*.WAD", SearchOption.AllDirectories))
            {
                Container container = new Container(fileName);
                foreach (KeyValuePair<string, IContainerEntry> entry in container.Entries)
                {
                    if (Path.GetExtension(entry.Key).ToUpper() == "." + extension)
                    {
                        Console.Write($"\t\tLoading {Path.Combine(fileName, entry.Key)}...");
                        using (Stream stream = entry.Value.GetStream())
                        {
                            LoadFile(stream, callback);
                        }
                    }
                }
            }
        }

        private static void LoadFile(Stream stream, Action<Stream> callback)
        {
            _stopwatch.Restart();
            callback?.Invoke(stream);
            _stopwatch.Stop();
            Console.WriteLine($" {_stopwatch.ElapsedMilliseconds}ms");
        }

        private static void ExtractPix(PixFile pixFile, string sourceName)
        {
            string extractPath = sourceName.Replace(_dataPath, _extractPath);
            if (pixFile.Images.Count == 1)
            {
                // If there is only one image in the PIX file, extract it as one file.
                extractPath = Path.GetDirectoryName(extractPath);
                Directory.CreateDirectory(extractPath);
                PixImage pixImage = pixFile.Images[0];
                string pngFileName = Path.ChangeExtension(GetValidFileName(pixImage.Name), "png");
                ConvertPixImage(pixImage, Path.Combine(extractPath, pngFileName));
            }
            else
            {
                // Extract PIX files with multiple images in their own folder.
                Directory.CreateDirectory(extractPath);
                foreach (PixImage pixImage in pixFile.Images)
                {
                    string pngFileName = Path.ChangeExtension(GetValidFileName(pixImage.Name), "png");
                    ConvertPixImage(pixImage, Path.Combine(extractPath, pngFileName));
                }
            }
        }

        private static void ExtractPackedPix(string fileName)
        {
            Container container = new Container(fileName);
            foreach (KeyValuePair<string, IContainerEntry> entry in container.Entries)
            {
                // Check if this is a PIX image.
                if (!entry.Key.EndsWith("pix", StringComparison.OrdinalIgnoreCase)) continue;
                // Decompress the entry in memory.
                PixFile pixFile = null;
                using (MemoryStream stream = new MemoryStream())
                {
                    using (Stream entryStream = entry.Value.GetStream())
                    {
                        entryStream.CopyTo(stream);
                    }
                    stream.Position = 0;
                    try
                    {
                        pixFile = new PixFile(stream);
                    }
                    catch (Exception ex)
                    {
                        // FIREBALL2.PIX isn't actually a PIX image.
                        Console.WriteLine($"{entry.Key}: {ex.Message}");
                    }
                }
                if (pixFile != null)
                {
                    ExtractPix(pixFile, Path.Combine(fileName, entry.Key));
                }
            }
        }

        private static string GetValidFileName(string path)
        {
            char[] invalidChars = Path.GetInvalidFileNameChars();
            foreach (char c in invalidChars)
            {
                path = path.Replace(c.ToString(), $"({(int)c})");
            }
            return path;
        }

        private static void ConvertPixImage(PixImage pixImage, string fileName)
        {
            // Create an ImageSharp instance out of the PIX image data.
            using (var image = new Image<Rgba32>(pixImage.Size.X, pixImage.Size.Y))
            {
                for (int y = 0; y < pixImage.Size.Y; y++)
                {
                    Span<Rgba32> row = image.GetRowSpan(y);
                    for (int x = 0; x < pixImage.Size.X; x++)
                    {
                        Color color = pixImage.GetColor(x, y);
                        row[x] = new Rgba32(color.R, color.G, color.B);
                    }
                }
                // Save it under the given file name.
                using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    image.SaveAsPng(stream);
                }
            }
        }
    }
}
