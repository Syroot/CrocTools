﻿using System;
using System.IO;
using Syroot.CrocTools.Core;

namespace Syroot.CrocTools.Containers
{
    /// <summary>
    /// Represents a collection of methods for compressing and decompressing Croc game data.
    /// </summary>
    public static class Compression
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _bufferSize = 81920;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Decompresses the data from the given <paramref name="input"/> stream in the <paramref name="output"/>
        /// stream.
        /// </summary>
        /// <param name="input">The input <see cref="Stream"/> providing the (possibly) compressed data.</param>
        /// <param name="output">The output <see cref="Stream"/> receiving the (possibly) decompressed data.</param>
        /// <param name="size">The (possibly) compressed size of the data in the input stream.</param>
        /// <param name="mode">The compression mode in which data is available in the input stream.</param>
        public static void Decompress(Stream input, Stream output, int size, CompressionMode mode)
        {
            switch (mode)
            {
                case CompressionMode.Uncompressed:
                    input.CopyTo(output, _bufferSize, size);
                    break;
                case CompressionMode.Bytes:
                    DecompressBytes(input, output, size);
                    break;
                case CompressionMode.Words:
                    DecompressWords(input, output, size);
                    break;
                default:
                    throw new ArgumentException($"Invalid {nameof(CompressionMode)} {mode}.");
            }
        }

        /// <summary>
        /// Decompresses the data from the given <paramref name="input"/> stream in the <paramref name="output"/>
        /// stream, which has been compressed using the <see cref="CompressionMode.Bytes"/> methods.
        /// </summary>
        /// <param name="input">The input <see cref="Stream"/> providing the compressed data.</param>
        /// <param name="output">The output <see cref="Stream"/> receiving the decompressed data.</param>
        /// <param name="size">The compressed size of the data in the input stream.</param>
        public static void DecompressBytes(Stream input, Stream output, int size)
        {
            long endPosition = input.Position + size;
            while (input.Position < endPosition)
            {
                sbyte command = (sbyte)input.ReadByte();
                if (command < 0)
                {
                    // Negative command, copy the given negated number of bytes.
                    input.CopyTo(output, _bufferSize, -command);
                }
                else
                {
                    // Positive command, repeat the next byte the given number of times, at least thrice.
                    int count = command + 3;
                    int data = input.ReadByte();
                    for (int i = 0; i < count; i++)
                    {
                        output.WriteByte((byte)data);
                    }
                }
            }
        }

        /// <summary>
        /// Decompresses the data from the given <paramref name="input"/> stream in the <paramref name="output"/>
        /// stream, which has been compressed using the <see cref="CompressionMode.Words"/> methods.
        /// </summary>
        /// <param name="input">The input <see cref="Stream"/> providing the compressed data.</param>
        /// <param name="output">The output <see cref="Stream"/> receiving the decompressed data.</param>
        /// <param name="size">The compressed size of the data in the input stream.</param>
        public static void DecompressWords(Stream input, Stream output, int size)
        {
            long endPosition = input.Position + size;
            while (input.Position < endPosition)
            {
                sbyte command = (sbyte)input.ReadByte();
                if (command < 0)
                {
                    // Negative command, copy the given negated number of words.
                    input.CopyTo(output, _bufferSize, -command * 2);
                }
                else
                {
                    // Positive command, repeat the next word the given number of times, at least twice.
                    int count = command + 2;
                    int data1 = input.ReadByte();
                    int data2 = input.ReadByte();
                    for (int i = 0; i < count; i++)
                    {
                        output.WriteByte((byte)data1);
                        output.WriteByte((byte)data2);
                    }
                }
            }
        }
    }
    
    /// <summary>
    /// Represents the data compression modes used by Croc game data.
    /// </summary>
    public enum CompressionMode
    {
        /// <summary>
        /// The data is compressed byte-wise.
        /// </summary>
        Bytes = 'b',

        /// <summary>
        /// The data is not compressed.
        /// </summary>
        Uncompressed = 'u',

        /// <summary>
        /// The data is compressed word-wise.
        /// </summary>
        Words = 'w'
    }
}
