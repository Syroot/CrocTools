﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Syroot.BinaryData;

namespace Syroot.CrocTools.Containers
{
    /// <summary>
    /// Represents a data container file which stores several different files in itself, referenced through a table of
    /// contents stored in an index file.
    /// </summary>
    public class Container
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        public const string IndexFileExtension = "idx";
        public const string DataFileExtension = "wad";

        private static readonly char[] _indexSeparator = new[] { ',' };
        
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Container"/> class.
        /// </summary>
        public Container()
        {
            Reset();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Container"/> class from the index or data file with the
        /// given <paramref name="fileName"/>. The file extension does not matter.
        /// </summary>
        /// <param name="fileName">The name of the file to load the container from, with or without file extension.
        /// </param>
        public Container(string fileName)
        {
            Load(fileName);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the dictionary mapping file names to <see cref="IContainerEntry"/> instances retrieving the data.
        /// </summary>
        public SortedDictionary<string, IContainerEntry> Entries { get; private set; }
        
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the instance from the index or data file with the given <paramref name="fileName"/>. The file
        /// extension does not matter.
        /// </summary>
        /// <param name="fileName">The name of the file to load the container from, with or without file extension.
        /// </param>
        public void Load(string fileName)
        {
            Reset();

            // Check if both index and data files exist.
            string indexFileName = Path.ChangeExtension(fileName, IndexFileExtension);
            string dataFileName = Path.ChangeExtension(fileName, DataFileExtension);
            if (!File.Exists(indexFileName))
                throw new InvalidOperationException($"Index file \"{indexFileName}\" does not exist.");
            if (!File.Exists(dataFileName))
                throw new InvalidOperationException($"Data file \"{dataFileName}\" does not exist.");

            // Load the index entries.
            using (StreamReader reader = new StreamReader(
                new FileStream(indexFileName, FileMode.Open, FileAccess.Read, FileShare.Read), Encoding.ASCII))
            {
                while (!reader.EndOfStream)
                {
                    string[] lineParts = reader.ReadLine().Split(_indexSeparator);
                    string name = lineParts[0];
                    int offset = int.Parse(lineParts[1]);
                    int size = int.Parse(lineParts[2]);
                    int decompressedSize = int.Parse(lineParts[3]);
                    CompressionMode mode = (CompressionMode)lineParts[4][0];
                    Entries.Add(name, new ContainerSourceEntry(dataFileName, offset, size, decompressedSize, mode));
                }
            }
        }

        /// <summary>
        /// Packs the content in this container and stores it in a new index and data file with the given
        /// <paramref name="fileName"/>, which file extension is changed accordingly.
        /// </summary>
        /// <param name="fileName">The name of the file to store the container in, with or without file extension.
        /// </param>
        public void Save(string fileName)
        {
            string indexFileName = Path.ChangeExtension(fileName, IndexFileExtension);
            string dataFileName = Path.ChangeExtension(fileName, DataFileExtension);
            using (StreamWriter indexWriter = new StreamWriter(
                new FileStream(indexFileName, FileMode.Create, FileAccess.Write, FileShare.Read), Encoding.ASCII))
            {
                using (BinaryDataWriter dataWriter = new BinaryDataWriter(
                    new FileStream(dataFileName, FileMode.Create, FileAccess.Write, FileShare.Read)))
                {
                    long currentStart = 0;
                    foreach (KeyValuePair<string, IContainerEntry> entry in Entries)
                    {
                        // Only write uncompressed data for now.
                        entry.Value.GetStream().CopyTo(dataWriter.BaseStream);
                        long length = dataWriter.Position - currentStart;
                        indexWriter.WriteLine(String.Join(_indexSeparator[0].ToString(), entry.Key, currentStart,
                            length, length, (char)CompressionMode.Uncompressed));
                        currentStart = dataWriter.Position;
                    }
                }
            }
        }

        /// <summary>
        /// Adds all files in the given <paramref name="directory"/> to the container. Existing entries are not
        /// destroyed.
        /// </summary>
        /// <param name="directory">The path of the directory to retrieve all files from.</param>
        public void PackAll(string directory)
        {
            foreach (string file in Directory.GetFiles(directory))
            {
                string fileName = Path.GetFileName(file);
                Entries.Add(fileName, new ContainerFileEntry(file));
            }
        }

        /// <summary>
        /// Extracts the data of all entries into their corresponding file into the given <paramref name="directory"/>.
        /// </summary>
        /// <param name="directory">The path of the directory to extract all files to.</param>
        public void UnpackAll(string directory)
        {
            foreach (KeyValuePair<string, IContainerEntry> entry in Entries)
            {
                string fileName = Path.Combine(directory, entry.Key);
                using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    entry.Value.GetStream().CopyTo(stream);
                }
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Reset()
        {
            Entries = new SortedDictionary<string, IContainerEntry>(StringComparer.OrdinalIgnoreCase);
        }
    }
}
