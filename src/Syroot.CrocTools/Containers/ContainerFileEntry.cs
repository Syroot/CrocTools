﻿using System.IO;

namespace Syroot.CrocTools.Containers
{
    /// <summary>
    /// Represents a <see cref="Container"/> entry which data is read from a file.
    /// </summary>
    public class ContainerFileEntry : IContainerEntry
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private string _fileName;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ContainerFileEntry"/> class referencing data from the file with
        /// the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file storing the data of this entry.</param>
        public ContainerFileEntry(string fileName)
        {
            _fileName = fileName;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------
        
        /// <summary>
        /// Returns a stream which can be used to read the data of the container entry.
        /// </summary>
        /// <returns>The <see cref="Stream"/> which can be used to read the data of the container entry.</returns>
        public Stream GetStream()
        {
            return new FileStream(_fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
        }
    }
}
