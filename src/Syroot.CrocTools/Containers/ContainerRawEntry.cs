﻿using System.IO;

namespace Syroot.CrocTools.Containers
{
    /// <summary>
    /// Represents a <see cref="Container"/> entry which data is stored in memory.
    /// </summary>
    public class ContainerRawEntry : IContainerEntry
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private byte[] _data;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ContainerRawEntry"/> class referencing the given
        /// <paramref name="data"/>.
        /// </summary>
        /// <param name="data">The data to reference.</param>
        public ContainerRawEntry(byte[] data)
        {
            _data = data;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Returns a stream which can be used to read the data of the container entry.
        /// </summary>
        /// <returns>The <see cref="Stream"/> which can be used to read the data of the container entry.</returns>
        public Stream GetStream()
        {
            return new MemoryStream(_data, false);
        }
    }
}
