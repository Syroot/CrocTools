﻿using System.IO;

namespace Syroot.CrocTools.Containers
{
    /// <summary>
    /// Represents a <see cref="Container"/> entry which data is read from a <see cref="Container"/>.
    /// </summary>
    public class ContainerSourceEntry : IContainerEntry
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------
        
        private string _dataFileName;
        private int _offset;
        private int _size;
        private int _decompressedSize;
        private CompressionMode _compressionMode;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ContainerSourceEntry"/> class referencing data from the given
        /// the file with the given <paramref name="dataFileName"/>.
        /// </summary>
        /// <param name="dataFileName">The file storing the container data.</param>
        /// <param name="offset">The start offset of the entry data in the container.</param>
        /// <param name="size">The length of the entry data in bytes.</param>
        /// <param name="decompressedSize">The decompressed length of the entry.</param>
        /// <param name="mode">The <see cref="CompressionMode"/> used on the data.</param>
        internal ContainerSourceEntry(string dataFileName, int offset, int size, int decompressedSize,
            CompressionMode mode)
        {
            _dataFileName = dataFileName;
            _offset = offset;
            _size = size;
            _decompressedSize = decompressedSize;
            _compressionMode = mode;
        }
        
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Returns a stream which can be used to read the data of the container entry.
        /// </summary>
        /// <returns>The <see cref="Stream"/> which can be used to read the data of the container entry.</returns>
        public Stream GetStream()
        {
            MemoryStream memoryStream = new MemoryStream(_decompressedSize);
            using (FileStream stream = new FileStream(_dataFileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                stream.Position = _offset;
                Compression.Decompress(stream, memoryStream, _size, _compressionMode);
            }
            memoryStream.Position = 0;
            return memoryStream;
        }
    }
}
