﻿using System.IO;

namespace Syroot.CrocTools.Containers
{
    /// <summary>
    /// Represents the common interface of entries in a <see cref="Container"/>, which can have different data
    /// sources.
    /// </summary>
    public interface IContainerEntry
    {
        // ---- METHODS ------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns a stream which can be used to read the data of the container entry.
        /// </summary>
        /// <returns>The <see cref="Stream"/> which can be used to read the data of the container entry.</returns>
        Stream GetStream();
    }
}
