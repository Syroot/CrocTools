﻿using System.Collections.Generic;
using Syroot.BinaryData;
using Syroot.CrocTools.Maths;

namespace Syroot.CrocTools.Core
{
    /// <summary>
    /// Represents a collection of extension methods for the <see cref="BinaryDataReader"/> class.
    /// </summary>
    internal static class BinaryDataReaderExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static Decimal4x12 ReadDecimal4x12(this BinaryDataReader self)
        {
            return new Decimal4x12(self.ReadInt16());
        }

        internal static IEnumerable<Decimal4x12> ReadDecimal4x12s(this BinaryDataReader self, int count)
        {
            while (count-- > 0)
            {
                yield return new Decimal4x12(self.ReadInt16());
            }
        }

        internal static Decimal20x12 ReadDecimal20x12(this BinaryDataReader self)
        {
            return new Decimal20x12(self.ReadInt32());
        }

        internal static Vector3Byte ReadVector3Byte(this BinaryDataReader self)
        {
            return new Vector3Byte(self.ReadByte(), self.ReadByte(), self.ReadByte());
        }

        internal static Vector3Int16 ReadVector3Int16(this BinaryDataReader self)
        {
            return new Vector3Int16(self.ReadInt16(), self.ReadInt16(), self.ReadInt16());
        }

        internal static Vector3Decimal4x12 ReadVector3Decimal4x12(this BinaryDataReader self)
        {
            return new Vector3Decimal4x12(self.ReadDecimal4x12(), self.ReadDecimal4x12(), self.ReadDecimal4x12());
        }

        internal static Vector3Decimal20x12 ReadVector3Decimal20x12(this BinaryDataReader self)
        {
            return new Vector3Decimal20x12(self.ReadDecimal20x12(), self.ReadDecimal20x12(), self.ReadDecimal20x12());
        }

        internal static Vector4Byte ReadVector4Byte(this BinaryDataReader self)
        {
            return new Vector4Byte(self.ReadByte(), self.ReadByte(), self.ReadByte(), self.ReadByte());
        }

        internal static Vector4Decimal4x12 ReadVector4Decimal4x12(this BinaryDataReader self)
        {
            return new Vector4Decimal4x12(self.ReadDecimal4x12(), self.ReadDecimal4x12(), self.ReadDecimal4x12(),
                self.ReadDecimal4x12());
        }

        internal static IEnumerable<Vector4Decimal4x12> ReadVector4Decimal4x12s(this BinaryDataReader self, int count)
        {
            while (count-- > 0)
            {
                yield return self.ReadVector4Decimal4x12();
            }
        }
    }
}
