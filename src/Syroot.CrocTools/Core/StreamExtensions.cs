﻿using System;
using System.IO;

namespace Syroot.CrocTools.Core
{
    /// <summary>
    /// Represents extension methods for the <see cref="Stream"/> class.
    /// </summary>
    internal static class StreamExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static void CopyTo(this Stream self, Stream stream, int bufferSize, int count)
        {
            if (count == 0) return;

            byte[] buffer = new byte[bufferSize];
            while (count > 0)
            {
                int bytesToRead = Math.Min(buffer.Length, count);
                if (self.Read(buffer, 0, Math.Min(buffer.Length, count)) != bytesToRead)
                {
                    throw new IOException("Could not read requested number of bytes.");
                }
                stream.Write(buffer, 0, bytesToRead);
                count -= bytesToRead;
            }
        }
    }
}
