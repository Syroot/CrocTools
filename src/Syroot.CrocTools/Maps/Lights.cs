﻿using Syroot.CrocTools.Maths;

namespace Syroot.CrocTools.Maps
{
    public struct Light
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public Vector3Decimal4x12 Position;
        public Vector4Byte Color;
        public uint FadeFrom;
        public uint FadeTo;
        public uint Unknown1;
        public ushort Unknown2;
    }

    public struct UniLight
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public Vector4Decimal4x12 Rotation;
        public Vector4Byte Color;
    }
}
