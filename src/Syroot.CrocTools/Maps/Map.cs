﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Syroot.BinaryData;
using Syroot.CrocTools.Core;
using Syroot.CrocTools.Maths;

namespace Syroot.CrocTools.Maps
{
    /// <summary>
    /// Represents a Croc level.
    /// </summary>
    public class Map
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _uniLightCount = 3;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Map"/> class.
        /// </summary>
        public Map()
        {
            Reset();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Map"/> class from the file with the given
        /// <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to create the instance from.</param>
        public Map(string fileName)
        {
            Load(fileName);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Map"/> class from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to create the instance from.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after creating the instance.
        /// </param>
        public Map(Stream stream, bool leaveOpen = false)
        {
            Load(stream, leaveOpen);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public ushort Version1 { get; set; }

        public ushort Version2 { get; set; }

        public string FileName { get; set; }

        public Vector3Int16 Size { get; set; }

        public Tileset Tileset { get; set; }

        public uint Flags { get; set; }

        public uint MusicTrack { get; set; }

        public uint SkyDome { get; set; }

        public uint Effect { get; set; }

        public uint Wait { get; set; }

        public uint AmbientSound { get; set; }
        
        public string Name { get; set; }

        public Decimal4x12 StartRotationY { get; set; }

        public List<Tile> Tiles { get; set; }

        public List<Strat> Strats { get; set; }

        public List<Portal> Portals { get; set; }

        public List<Light> Lights { get; set; }

        public UniLight[] UniLights { get; private set; }

        public Vector4Byte AmbientLight { get; set; }
        
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the instance data from the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the instance data from.</param>
        public void Load(string fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                Load(fileStream);
            }
        }

        /// <summary>
        /// Loads the instance data from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the instance data from.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after loading the instance.
        /// </param>
        public void Load(Stream stream, bool leaveOpen = false)
        {
            Reset();
            using (BinaryDataReader reader = new BinaryDataReader(stream, Encoding.ASCII, leaveOpen))
            {
                // Read general properties.
                uint fileSize = reader.ReadUInt32();
                Version1 = reader.ReadUInt16();
                Version2 = reader.ReadUInt16();
                FileName = reader.ReadString(BinaryStringFormat.ByteLengthPrefix);
                ushort totalWaypointCount = reader.ReadUInt16();
                Size = reader.ReadVector3Int16();
                Tileset = reader.ReadEnum<Tileset>(true);
                Flags = reader.ReadUInt32();
                MusicTrack = reader.ReadUInt32();
                SkyDome = reader.ReadUInt32();
                Effect = reader.ReadUInt32();
                Wait = reader.ReadUInt32();
                AmbientSound = reader.ReadUInt32();
                reader.Seek(sizeof(ushort)); // Reserved
                ushort tileCount = reader.ReadUInt16();
                Name = reader.ReadString(BinaryStringFormat.ByteLengthPrefix);
                StartRotationY = reader.ReadDecimal4x12();

                // Read tiles.
                Tiles.Capacity = tileCount;
                while (tileCount-- > 0)
                {
                    Tile tile = new Tile()
                    {
                        Position = reader.ReadVector3Int16(),
                        RotationY = reader.ReadDecimal4x12(),
                        ID = reader.ReadUInt16()
                    };
                    if (Version1 > 20)
                    {
                        tile.ZoneActive = reader.ReadUInt16();
                        tile.ZoneMember = reader.ReadUInt16();
                    }
                    Tiles.Add(tile);
                }

                // Read strats.
                ushort stratCount = reader.ReadUInt16();
                Strats.Capacity = stratCount;
                while (stratCount-- > 0)
                {
                    Strat strat = new Strat()
                    {
                        Parameters = reader.ReadUInt32s(8),
                        Position = reader.ReadVector3Decimal20x12(),
                        Rotation = reader.ReadVector3Decimal4x12(),
                        Name = reader.ReadString(BinaryStringFormat.ByteLengthPrefix)
                    };
                    // Read waypoints.
                    ushort waypointCount = reader.ReadUInt16();
                    strat.Waypoints = new List<Waypoint>(waypointCount);
                    while (waypointCount-- > 0)
                    {
                        strat.Waypoints.Add(new Waypoint()
                        {
                            Position = reader.ReadVector3Decimal20x12(),
                            Variable = reader.ReadUInt32()
                        });
                    }
                    Strats.Add(strat);
                }

                // Read portals.
                ushort portalCount = reader.ReadUInt16();
                Portals.Capacity = portalCount;
                while (portalCount-- > 0)
                {
                    Portals.Add(new Portal()
                    {
                        Position = reader.ReadVector3Int16(),
                        Level = reader.ReadUInt16(),
                        SubLevel = reader.ReadUInt16(),
                        Flags = reader.ReadEnum<PortalFlags>(true),
                        ID = reader.ReadUInt16()
                    });
                }

                // Read lights.
                ushort lightCount = reader.ReadUInt16();
                Lights.Capacity = lightCount;
                while (lightCount-- > 0)
                {
                    Lights.Add(new Light()
                    {
                        Position = reader.ReadVector3Decimal4x12(),
                        Color = reader.ReadVector4Byte(),
                        FadeFrom = reader.ReadUInt32(),
                        FadeTo = reader.ReadUInt32(),
                        Unknown1 = reader.ReadUInt32(),
                        Unknown2 = reader.ReadUInt16()
                    });
                }

                // Read UniLights.
                UniLights = new UniLight[_uniLightCount];
                for (int i = 0; i < _uniLightCount; i++)
                {
                    UniLights[i] = new UniLight()
                    {
                        Rotation = reader.ReadVector4Decimal4x12(),
                        Color = reader.ReadVector4Byte()
                    };
                }

                // Read ambient light.
                AmbientLight = reader.ReadVector4Byte();
                
                // 4 bytes trash at the end.
                reader.Position += 4;
            }
        }

        /// <summary>
        /// Saves the instance data in the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to save the instance data in.</param>
        public void Save(string fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                Save(fileStream);
            }
        }

        /// <summary>
        /// Saves the instance data in the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to save the instance data in.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after saving the instance.
        /// </param>
        public void Save(Stream stream, bool leaveOpen = false)
        {
            using (BinaryDataWriter writer = new BinaryDataWriter(stream, leaveOpen))
            {
                // TODO
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Reset()
        {
            Version1 = default(ushort);
            Version2 = default(ushort);
            FileName = null;
            Size = default(Vector3Int16);
            Tileset = default(Tileset);
            Flags = default(uint);
            MusicTrack = default(uint);
            SkyDome = default(uint);
            Effect = default(uint);
            Wait = default(uint);
            AmbientSound = default(uint);
            Name = null;
            StartRotationY = default(Decimal4x12);
            Tiles = new List<Tile>();
            Strats = new List<Strat>();
            Portals = new List<Portal>();
            Lights = new List<Light>();
            UniLights = new UniLight[3];
            AmbientLight = default(Vector4Byte);
        }
    }
}
