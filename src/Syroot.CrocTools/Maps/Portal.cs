﻿using System;
using Syroot.CrocTools.Maths;

namespace Syroot.CrocTools.Maps
{
    public struct Portal
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public Vector3Int16 Position;
        public ushort Level;
        public ushort SubLevel;
        public PortalFlags Flags;
        public ushort ID;
    }

    [Flags]
    public enum PortalFlags : ushort
    {
        Walk = 1 << 0,
        Jump = 1 << 1,
        Land = 1 << 2,
        Initial = 1 << 3
    }
}
