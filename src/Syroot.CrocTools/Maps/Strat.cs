﻿using System.Collections.Generic;
using Syroot.CrocTools.Maths;

namespace Syroot.CrocTools.Maps
{
    public class Strat
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public uint[] Parameters { get; set; }

        public Vector3Decimal20x12 Position { get; set; }

        public Vector3Decimal4x12 Rotation { get; set; }
        
        public string Name { get; set; }

        public List<Waypoint> Waypoints { get; set; }
    }

    public struct Waypoint
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public Vector3Decimal20x12 Position;

        public uint Variable;
    }
}
