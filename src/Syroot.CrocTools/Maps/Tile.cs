﻿using Syroot.CrocTools.Maths;

namespace Syroot.CrocTools.Maps
{
    public struct Tile
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public Vector3Int16 Position;
        public Decimal4x12 RotationY;
        public ushort ID;
        public ushort ZoneActive;
        public ushort ZoneMember;
    }

    public enum Tileset : ushort
    {
        Ice,
        Water,
        Castle,
        Desert,
        Cave,
        Woods,
        Dungeon,
        BossFlibby,
        BossDemonItsy,
        BossCactusJack,
        BossFosley,
        BossNeptuna,
        BossDante,
        BossCrystal,
        BossFeeble,
        BossChumly,
        Whale,
        WorldForest,
        WorldIce,
        WorldDesert,
        WorldCastle,
        WorldCrystal,
        IceSlide,
        Boulder,
        IceCave
    }
}
