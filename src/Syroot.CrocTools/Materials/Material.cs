﻿using System;
using Syroot.Maths;

namespace Syroot.CrocTools.Materials
{
    /// <summary>
    /// Represents the visual properties of a surface and determines how it is drawn exactly.
    /// </summary>
    public class Material
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Identifier { get; set; }

        public MaterialFlags Flags { get; set; }

        public Color Color { get; set; }

        public float Ambient { get; set; }

        public float Diffuse { get; set; }

        public float Specular { get; set; }

        public float Power { get; set; }

        public int? IndexBase { get; set; }

        public int? IndexRange { get; set; }

        public string ColorMap { get; set; }

        public string IndexShade { get; set; }

        public byte? Opacity { get; set; }
    }

    [Flags]
    public enum MaterialFlags
    {
        None = 0,
        Light = 1 << 0,
        Smooth = 1 << 1,
        MapInterpolation = 1 << 2,
        Dither = 1 << 3,
        Perspective = 1 << 4,
        ForceFront = 1 << 5
    }
}