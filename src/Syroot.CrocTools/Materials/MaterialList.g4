﻿grammar MaterialList;

@parser::header {#pragma warning disable 3021}
@lexer::header {#pragma warning disable 3021}

file
	:	block*
	;

block
	:	ID '=' '[' property* ']' ';'
	;

property
	:	ID '=' expr ';'
	;

expr
	:	INT
	|	FLOAT
	|	STRING
	|	idArray
	|	intArray
	;

idArray
	:	'[' ']'
	|	'[' ID (',' ID)* ']'
	;

intArray
	:	'[' ']'
	|	'[' INT (',' INT)* ']'
	;

ID
	:	[a-zA-Z_]+
	;

INT
	:	DIGIT+
	;

FLOAT
	:	'.' DIGIT+
	|	DIGIT+ '.' DIGIT+
	;
	
DIGIT
	:	[0-9]
	;

STRING
	:	'"' ~('\r' | '\n' | '"')* '"' 
	;
	
WHITESPACE
	:	[ \t]+ -> skip
	;

COMMENT
	:	'#' ~('\r' | '\n' )* NEWLINE -> skip
	;
	
NEWLINE
	:	('\r\n' | '\r' | '\n' ) -> skip
	;
