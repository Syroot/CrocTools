﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Antlr4.Runtime.Tree;
using Syroot.Maths;
using static Syroot.CrocTools.Materials.MaterialListParser;

namespace Syroot.CrocTools.Materials
{
    /// <summary>
    /// Represents a parser of material files, creating <see cref="Material"/> instances out of it.
    /// </summary>
    internal class MaterialListVisitor : MaterialListBaseVisitor<object>
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------
        
        private Material _material;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public List<Material> Materials { get; private set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override object VisitFile(FileContext context)
        {
            // Go through all materials defined in the file.
            Materials = new List<Material>();
            foreach (BlockContext blockContext in context.block())
            {
                string blockType = blockContext.ID().GetText();
                switch (blockType)
                {
                    case "material":
                        VisitBlock(blockContext);
                        break;
                    default:
                        throw new InvalidDataException($"Unknown \"{blockType}\" block.");
                }
            }
            return true;
        }

        public override object VisitBlock(BlockContext context)
        {
            // Create the material and map the properties.
            _material = new Material();
            foreach (PropertyContext propertyContext in context.property())
            {
                VisitProperty(propertyContext);
            }
            Materials.Add(_material);
            return true;
        }

        public override object VisitProperty(PropertyContext context)
        {
            string propertyName = context.ID().GetText();
            switch (propertyName)
            {
                case "identifier":
                    _material.Identifier = GetStringProperty(context);
                    break;
                case "flags":
                    _material.Flags = GetMaterialFlagsProperty(context);
                    break;
                case "colour":
                    _material.Color = GetColorProperty(context);
                    break;
                case "ambient":
                    _material.Ambient = GetSingleProperty(context);
                    break;
                case "diffuse":
                    _material.Diffuse = GetSingleProperty(context);
                    break;
                case "specular":
                    _material.Specular = GetSingleProperty(context);
                    break;
                case "power":
                    _material.Power = GetSingleProperty(context);
                    break;
                case "index_base":
                    _material.IndexBase = GetInt32Property(context);
                    break;
                case "index_range":
                    _material.IndexRange = GetInt32Property(context);
                    break;
                case "colour_map":
                    _material.ColorMap = GetStringProperty(context);
                    break;
                case "index_shade":
                    _material.IndexShade = GetStringProperty(context);
                    break;
                case "opacity":
                    _material.Opacity = GetByteProperty(context);
                    break;
                default:
                    throw new InvalidDataException($"Unknown \"{propertyName}\" material property.");
            }
            return true;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private byte GetByteProperty(PropertyContext context)
        {
            return Byte.Parse(context.expr().INT().GetText(), CultureInfo.InvariantCulture);
        }

        private Color GetColorProperty(PropertyContext context)
        {
            ITerminalNode[] tokens = context.expr().intArray().GetTokens(INT);
            byte[] values = tokens.Select((x) => Byte.Parse(x.GetText(), CultureInfo.InvariantCulture)).ToArray();
            return new Color(values[0], values[1], values[2]);
        }

        private int GetInt32Property(PropertyContext context)
        {
            return Int32.Parse(context.expr().INT().GetText(), CultureInfo.InvariantCulture);
        }

        private MaterialFlags GetMaterialFlagsProperty(PropertyContext context)
        {
            ITerminalNode[] tokens = context.expr().idArray().GetTokens(ID);
            string[] values = tokens.Select((x) => x.GetText()).ToArray();
            MaterialFlags flags = MaterialFlags.None;
            foreach (string value in values)
            {
                flags |= GetMaterialFlag(value);
            }
            return flags;
        }

        private float GetSingleProperty(PropertyContext context)
        {
            return Single.Parse(context.expr().FLOAT().GetText(), CultureInfo.InvariantCulture);
        }

        private string GetStringProperty(PropertyContext context)
        {
            return context.expr().STRING().GetText().Replace("\"", String.Empty);
        }

        private MaterialFlags GetMaterialFlag(string value)
        {
            switch (value)
            {
                case "light":
                    return MaterialFlags.Light;
                case "smooth":
                    return MaterialFlags.Smooth;
                case "map_interpolation":
                    return MaterialFlags.MapInterpolation;
                case "dither":
                    return MaterialFlags.Dither;
                case "perspective":
                    return MaterialFlags.Perspective;
                case "force_front":
                    return MaterialFlags.ForceFront;
                default:
                    throw new InvalidDataException($"Unknown material flag {value}.");
            }
        }
    }
}
