﻿using System;

namespace Syroot.CrocTools.Maths
{
    /// <summary>
    /// Represents a 16-bit fixed-point decimal consisting of 1 sign, 3 integral and 12 fractional bits (denoted as
    /// Q4.12). Note that the implementation is not reporting over- and underflowing errors.
    /// </summary>
    public struct Decimal4x12
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Represents the largest possible value of <see cref="Decimal4x12"/>.
        /// </summary>
        public static readonly Decimal4x12 MaxValue = new Decimal4x12(Int16.MaxValue);

        /// <summary>
        /// Represents the smallest possible value of <see cref="Decimal4x12"/>.
        /// </summary>
        public static readonly Decimal4x12 MinValue = new Decimal4x12(Int16.MinValue);
        
        private const int _m = 4; // Number of integral part bits.
        private const int _n = 12; // Number of fractional part bits.
        
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Decimal4x12"/> struct from the given <paramref name="raw"/>
        /// representation.
        /// </summary>
        /// <param name="raw">The raw representation of the internally stored bits.</param>
        internal Decimal4x12(int raw)
        {
            unchecked { Raw = (short)raw; }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the internally stored value to represent the instance.
        /// </summary>
        /// <remarks>Signed to get arithmetic rather than logical shifts.</remarks>
        internal short Raw { get; private set; }

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the given <see cref="Decimal4x12"/>.
        /// </summary>
        /// <param name="a">The <see cref="Decimal4x12"/>.</param>
        /// <returns>The result.</returns>
        public static Decimal4x12 operator +(Decimal4x12 a)
        {
            return a;
        }

        /// <summary>
        /// Adds the first <see cref="Decimal4x12"/> to the second one.
        /// </summary>
        /// <param name="a">The first <see cref="Decimal4x12"/>.</param>
        /// <param name="b">The second <see cref="Decimal4x12"/>.</param>
        /// <returns>The addition result.</returns>
        public static Decimal4x12 operator +(Decimal4x12 a, Decimal4x12 b)
        {
            return new Decimal4x12(a.Raw + b.Raw);
        }

        /// <summary>
        /// Negates the given <see cref="Decimal4x12"/>.
        /// </summary>
        /// <param name="a">The <see cref="Decimal4x12"/> to negate.</param>
        /// <returns>The negated result.</returns>
        public static Decimal4x12 operator -(Decimal4x12 a)
        {
            return new Decimal4x12(-a.Raw);
        }

        /// <summary>
        /// Subtracts the first <see cref="Decimal4x12"/> from the second one.
        /// </summary>
        /// <param name="a">The first <see cref="Decimal4x12"/>.</param>
        /// <param name="b">The second <see cref="Decimal4x12"/>.</param>
        /// <returns>The subtraction result.</returns>
        public static Decimal4x12 operator -(Decimal4x12 a, Decimal4x12 b)
        {
            return new Decimal4x12(a.Raw - b.Raw);
        }

        /// <summary>
        /// Multiplicates the given <see cref="Decimal4x12"/> by the scalar.
        /// </summary>
        /// <param name="a">The <see cref="Decimal4x12"/>.</param>
        /// <param name="s">The scalar.</param>
        /// <returns>The multiplication result.</returns>
        public static Decimal4x12 operator *(Decimal4x12 a, int s)
        {
            return new Decimal4x12(a.Raw * s);
        }

        /// <summary>
        /// Multiplicates the first <see cref="Decimal4x12"/> by the second one.
        /// </summary>
        /// <param name="a">The first <see cref="Decimal4x12"/>.</param>
        /// <param name="b">The second <see cref="Decimal4x12"/>.</param>
        /// <returns>The multiplication result.</returns>
        public static Decimal4x12 operator *(Decimal4x12 a, Decimal4x12 b)
        {
            int k = 1 << (_n - 1);
            return new Decimal4x12((a.Raw * b.Raw + k) >> _n);
        }

        /// <summary>
        /// Divides the given <see cref="Decimal4x12"/> through the scalar.
        /// </summary>
        /// <param name="a">The <see cref="Decimal4x12"/>.</param>
        /// <param name="s">The scalar.</param>
        /// <returns>The division result.</returns>
        public static Decimal4x12 operator /(Decimal4x12 a, int s)
        {
            return new Decimal4x12(a.Raw / s);
        }

        /// <summary>
        /// Divides the first <see cref="Decimal4x12"/> through the second one.
        /// </summary>
        /// <param name="a">The first <see cref="Decimal4x12"/>.</param>
        /// <param name="b">The second <see cref="Decimal4x12"/>.</param>
        /// <returns>The division result.</returns>
        public static Decimal4x12 operator /(Decimal4x12 a, Decimal4x12 b)
        {
            return new Decimal4x12((a.Raw << _n) / b.Raw);
        }

        /// <summary>
        /// Gets a value indicating whether the first specified <see cref="Decimal4x12"/> is the same as the second
        /// specified <see cref="Decimal4x12"/>.
        /// </summary>
        /// <param name="a">The first <see cref="Decimal4x12"/> to compare.</param>
        /// <param name="b">The second <see cref="Decimal4x12"/> to compare.</param>
        /// <returns>true, if both <see cref="Decimal4x12"/> are the same.</returns>
        public static bool operator ==(Decimal4x12 a, Decimal4x12 b)
        {
            return a.Equals(b);
        }

        /// <summary>
        /// Gets a value indicating whether the first specified <see cref="Decimal4x12"/> is not the same as the second
        /// specified <see cref="Decimal4x12"/>.
        /// </summary>
        /// <param name="a">The first <see cref="Decimal4x12"/> to compare.</param>
        /// <param name="b">The second <see cref="Decimal4x12"/> to compare.</param>
        /// <returns>true, if both <see cref="Decimal4x12"/> are not the same.</returns>
        public static bool operator !=(Decimal4x12 a, Decimal4x12 b)
        {
            return !a.Equals(b);
        }
        
        /// <summary>
        /// Converts the given <paramref name="value"/> to a <see cref="Decimal4x12"/> instance.
        /// </summary>
        /// <param name="value">The <see cref="Int32"/> value to represent in the new <see cref="Decimal4x12"/>
        /// instance.</param>
        public static explicit operator Decimal4x12(Int32 value)
        {
            return new Decimal4x12(value << _n);
        }

        /// <summary>
        /// Converts the given <paramref name="value"/> to a <see cref="Decimal4x12"/> instance.
        /// </summary>
        /// <param name="value">The <see cref="Double"/> value to represent in the new <see cref="Decimal4x12"/>
        /// instance.</param>
        public static explicit operator Decimal4x12(Double value)
        {
            return new Decimal4x12((int)Math.Round(value * (1 << _n)));
        }

        /// <summary>
        /// Converts the given <paramref name="value"/> to a <see cref="Decimal4x12"/> instance.
        /// </summary>
        /// <param name="value">The <see cref="Single"/> value to represent in the new <see cref="Decimal4x12"/>
        /// instance.</param>
        public static explicit operator Decimal4x12(Single value)
        {
            return new Decimal4x12((int)Math.Round(value * (1 << _n)));
        }

        /// <summary>
        /// Converts the given <paramref name="value"/> to a <see cref="Double"/> instance.
        /// </summary>
        /// <param name="value">The <see cref="Decimal4x12"/> value to represent in the new <see cref="Double"/>
        /// instance.</param>
        public static explicit operator Double(Decimal4x12 value)
        {
            return (double)value.Raw / (1 << _n);
        }

        /// <summary>
        /// Converts the given <paramref name="value"/> to a <see cref="Int32"/> instance.
        /// </summary>
        /// <param name="value">The <see cref="Decimal4x12"/> value to represent in the new <see cref="Int32"/>
        /// instance.</param>
        public static explicit operator Int32(Decimal4x12 value)
        {
            int k = 1 << (_n - 1);
            return (value.Raw + k) >> _n;
        }

        /// <summary>
        /// Converts the given <paramref name="value"/> to a <see cref="Single"/> instance.
        /// </summary>
        /// <param name="value">The <see cref="Decimal4x12"/> value to represent in the new <see cref="Single"/>
        /// instance.</param>
        public static explicit operator Single(Decimal4x12 value)
        {
            return (float)((double)value.Raw / (1 << _n));
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether this <see cref="Decimal4x12"/> is the same as the second specified
        /// <see cref="Decimal4x12"/>.
        /// </summary>
        /// <param name="obj">The object to compare, if it is a <see cref="Decimal4x12"/>.</param>
        /// <returns>true, if both <see cref="Decimal4x12"/> are the same.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Decimal4x12))
            {
                return false;
            }
            Decimal4x12 decimal5x11 = (Decimal4x12)obj;
            return Equals(decimal5x11);
        }

        /// <summary>
        /// Gets a hash code as an indication for object equality.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            return Raw;
        }

        /// <summary>
        /// Gets a string describing this <see cref="Decimal4x12"/>.
        /// </summary>
        /// <returns>A string describing this <see cref="Decimal4x12"/>.</returns>
        public override string ToString()
        {
            return ((double)this).ToString();
        }

        /// <summary>
        /// Indicates whether the current <see cref="Decimal4x12"/> is equal to another <see cref="Decimal4x12"/>.
        /// </summary>
        /// <param name="other">A <see cref="Decimal4x12"/> to compare with this <see cref="Decimal4x12"/>.</param>
        /// <returns>true if the current <see cref="Decimal4x12"/> is equal to the other parameter; otherwise, false.
        /// </returns>
        public bool Equals(Decimal4x12 other)
        {
            return Equals(Raw == other.Raw);
        }
    }
}
