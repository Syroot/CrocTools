﻿namespace Syroot.CrocTools.Maths
{
    public struct Vector3Byte
    {
        public byte R, G, B;

        public Vector3Byte(byte r, byte g, byte b)
        {
            R = r; G = g; B = b;
        }
    }

    public struct Vector3Int16
    {
        public short X, Y, Z;

        public Vector3Int16(short x, short y, short z)
        {
            X = x; Y = y; Z = z;
        }
    }

    public struct Vector3Decimal4x12
    {
        public Decimal4x12 X, Y, Z;

        public Vector3Decimal4x12(Decimal4x12 x, Decimal4x12 y, Decimal4x12 z)
        {
            X = x; Y = y; Z = z;
        }
    }

    public struct Vector3Decimal20x12
    {
        public Decimal20x12 X, Y, Z;

        public Vector3Decimal20x12(Decimal20x12 x, Decimal20x12 y, Decimal20x12 z)
        {
            X = x; Y = y; Z = z;
        }
    }

    public struct Vector4Byte
    {
        public byte R, G, B, A;

        public Vector4Byte(byte r, byte g, byte b, byte a)
        {
            R = r; G = g; B = b; A = a;
        }
    }

    public struct Vector4Decimal4x12
    {
        public Decimal4x12 X, Y, Z, W;

        public Vector4Decimal4x12(Decimal4x12 x, Decimal4x12 y, Decimal4x12 z, Decimal4x12 w)
        {
            X = x; Y = y; Z = z; W = w;
        }
    }
}
