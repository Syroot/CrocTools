﻿using System;
using Syroot.CrocTools.Maths;

namespace Syroot.CrocTools.Models
{
    /// <summary>
    /// Represents a polygon forming the <see cref="Mesh"/> surface.
    /// </summary>
    public struct Face
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string MaterialName { get; set; }

        public Vector4Decimal4x12 Normal { get; set; }

        /// <summary>
        /// Gets or sets the indices of the vertices to reference. In case of a triangle, the face is formed by vertices
        /// 2, 1 and 0. In case of a quad, the second face is formed by vertices 2, 3 and 1.
        /// </summary>
        public ushort[] Indices { get; set;  }

        /// <summary>
        /// Gets or sets the color of the face. Only used when the face is not textured.
        /// </summary>
        public Vector3Byte Color { get; set; }

        /// <summary>
        /// Gets or sets flags describing how the face is formed and drawn.
        /// </summary>
        public FaceFlags Flags { get; set; }
    }

    [Flags]
    public enum FaceFlags : byte
    {
        /// <summary>
        /// The face is composed of four vertices rather than three.
        /// </summary>
        Quad = 1 << 3,
        
        /// <summary>
        /// The face is drawn with a texture rather than a color.
        /// </summary>
        Texture = 1 << 7 // TODO: Validate meaning.
    }
}
