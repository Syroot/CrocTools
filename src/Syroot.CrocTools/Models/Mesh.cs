﻿using Syroot.CrocTools.Maths;

namespace Syroot.CrocTools.Models
{
    /// <summary>
    /// Represents a net surface of vertices and faces forming the visible object.
    /// </summary>
    public class Mesh
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _boundingBoxVertices = 9;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Mesh()
        {
            BoundingBox = new Vector4Decimal4x12[_boundingBoxVertices];
        }
        
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Decimal20x12 Radius { get; set; }

        public Vector4Decimal4x12[] BoundingBox { get; set; }

        public Vector4Decimal4x12[] Vertices { get; set; }

        public Vector4Decimal4x12[] Normals { get; set; }

        public Face[] Faces { get; set; }

        public FaceCollision[] FaceCollisions { get; set; }
    }
}
