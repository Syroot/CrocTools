﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Syroot.BinaryData;
using Syroot.CrocTools.Core;

namespace Syroot.CrocTools.Models
{
    /// <summary>
    /// Represents 3-dimensional model data, typically stored in MOD files.
    /// </summary>
    public class Model
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class.
        /// </summary>
        public Model()
        {
            Reset();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class from the file with the given
        /// <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to create the instance from.</param>
        public Model(string fileName)
        {
            Load(fileName);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to create the instance from.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after creating the instance.
        /// </param>
        public Model(Stream stream, bool leaveOpen = false)
        {
            Load(stream);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public ModelFlags Flags { get; set; }

        /// <summary>
        /// Gets or sets the list of <see cref="Mesh"/> instances stored in this model.
        /// </summary>
        public List<Mesh> Meshes { get; set; }
        
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the instance data from the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the instance data from.</param>
        public void Load(string fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                Load(fileStream);
            }
        }

        /// <summary>
        /// Loads the instance data from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the instance data from.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after loading the instance.
        /// </param>
        public void Load(Stream stream, bool leaveOpen = false)
        {
            Reset();
            using (BinaryDataReader reader = new BinaryDataReader(stream, leaveOpen))
            {
                ushort meshCount = reader.ReadUInt16();
                Flags = reader.ReadEnum<ModelFlags>(true);

                // Read meshes.
                Meshes.Capacity = meshCount;
                while (meshCount-- > 0)
                {
                    Mesh mesh = new Mesh();
                    mesh.Radius = reader.ReadDecimal20x12();
                    mesh.BoundingBox = reader.ReadVector4Decimal4x12s(9).ToArray();

                    // Read vertices and normals.
                    int vertexCount = reader.ReadInt32();
                    mesh.Vertices = reader.ReadVector4Decimal4x12s(vertexCount).ToArray();
                    mesh.Normals = reader.ReadVector4Decimal4x12s(vertexCount).ToArray();

                    // Read faces.
                    int faceCount = reader.ReadInt32();
                    mesh.Faces = new Face[faceCount];
                    for (int i = 0; i < faceCount; i++)
                    {
                        mesh.Faces[i] = new Face()
                        {
                            MaterialName = reader.ReadString(64),
                            Normal = reader.ReadVector4Decimal4x12(),
                            Indices = reader.ReadUInt16s(4),
                            Color = reader.ReadVector3Byte(),
                            Flags = reader.ReadEnum<FaceFlags>(false)
                        };
                    }

                    // Read face collisions.
                    if (Flags.HasFlag(ModelFlags.HasCollision))
                    {
                        ushort floor = reader.ReadUInt16();
                        ushort ceil = reader.ReadUInt16();
                        int collisionCount = floor + ceil;
                        if (collisionCount > 0)
                        {
                            mesh.FaceCollisions = new FaceCollision[collisionCount];
                            for (int i = 0; i < collisionCount; i++)
                            {
                                mesh.FaceCollisions[0].Unknown = reader.ReadDecimal4x12s(22).ToArray();
                            }
                        }
                    }

                    Meshes.Add(mesh);
                }

                // 4 bytes trash at the end.
                reader.Position += 4;
            }
        }

        /// <summary>
        /// Saves the instance data in the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to save the instance data in.</param>
        public void Save(string fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                Save(fileStream);
            }
        }

        /// <summary>
        /// Saves the instance data in the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to save the instance data in.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after saving the instance.
        /// </param>
        public void Save(Stream stream, bool leaveOpen = false)
        {
            using (BinaryDataWriter writer = new BinaryDataWriter(stream, leaveOpen))
            {
                throw new NotImplementedException();
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Reset()
        {
            Meshes = new List<Mesh>();
            Flags = default(ushort);
        }
    }

    [Flags]
    public enum ModelFlags : ushort
    {
        None = 0,
        HasCollision = 1 << 0
    }
}
