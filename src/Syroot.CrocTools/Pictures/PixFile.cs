﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Syroot.BinaryData;

namespace Syroot.CrocTools.Pictures
{
    /// <summary>
    /// Represents a collection of 1 or more bitmaps, typically stored in PIX files.
    /// </summary>
    public class PixFile
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------
        
        private const int _minHeaderSize = 17;
        private static readonly int[] _validSignature = new[] { 0x12, 0x08, 0x02, 0x02 };

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="PixFile"/> class.
        /// </summary>
        public PixFile()
        {
            Reset();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PixFile"/> class from the file with the given
        /// <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to create the instance from.</param>
        public PixFile(string fileName)
        {
            Load(fileName);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PixFile"/> class from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to create the instance from.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after creating the instance.
        /// </param>
        public PixFile(Stream stream, bool leaveOpen = false)
        {
            Load(stream, leaveOpen);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the list of images stored in this file.
        /// </summary>
        public List<PixImage> Images { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the instance data from the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the instance data from.</param>
        public void Load(string fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                Load(fileStream);
            }
        }

        /// <summary>
        /// Loads the instance data from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the instance data from.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after loading the instance.
        /// </param>
        public void Load(Stream stream, bool leaveOpen = false)
        {
            Reset();
            using (BinaryDataReader reader = new BinaryDataReader(stream, Encoding.ASCII, leaveOpen))
            {
                // Read and check the signature.
                reader.ByteOrder = ByteOrder.BigEndian;
                for (int i = 0; i < _validSignature.Length; i++)
                {
                    if (reader.ReadInt32() != _validSignature[i])
                        throw new InvalidDataException("Invalid PIX signature.");
                }
                // Read header chunks and delegate further image loading to image subclasses.
                while (!reader.EndOfStream)
                {
                    // Some files have a trash byte at the end. Check if a full header could be read.
                    if (reader.Length - reader.Position < _minHeaderSize) break;
                    Images.Add(new PixImage(reader));
                }
            }
        }

        /// <summary>
        /// Saves the instance data in the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to save the instance data in.</param>
        public void Save(string fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                Save(fileStream);
            }
        }

        /// <summary>
        /// Saves the instance data in the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to save the instance data in.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after saving the instance.
        /// </param>
        public void Save(Stream stream, bool leaveOpen = false)
        {
            using (BinaryDataWriter writer = new BinaryDataWriter(stream, leaveOpen))
            {
                throw new NotImplementedException();
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Reset()
        {
            Images = new List<PixImage>();
        }
    }

    /// <summary>
    /// Represents the possible data formats in which pixel colors are stored.
    /// </summary>
    public enum PixFormat : byte
    {
        PAL8 = 3,
        RGB555_BE,
        RGB565_BE,
        RGB24,
        XRGB // Used by palettes
    }

    internal enum PixChunkType : int
    {
        Header1 = 0x03,
        Header2 = 0x3D,
        Data = 0x21
    }
}
