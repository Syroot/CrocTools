﻿using System;
using System.IO;
using Syroot.BinaryData;
using Syroot.Maths;

namespace Syroot.CrocTools.Pictures
{
    /// <summary>
    /// Represents an image stored in <see cref="PixFile"/> instances.
    /// </summary>
    public class PixImage
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="PixImage"/> class created from the given
        /// <paramref name="reader"/>.
        /// </summary>
        /// <param name="reader">The <see cref="BinaryDataReader"/> to create the instance from.</param>
        internal PixImage(BinaryDataReader reader)
        {
            // Read the image header.
            ReadHeaderChunk(reader, (x) =>
            {
                Format = x.Format;
                Size = x.Size;
                Name = x.Name;
            });

            // Read the palette if available.
            if (Format == PixFormat.PAL8)
            {
                // Read header and data of palette.
                ReadHeaderChunk(reader, (x) =>
                {
                    if (x.Format != PixFormat.XRGB)
                        throw new InvalidDataException("PIX palette stored in unsupported format, must be RGB data.");
                });
                ReadDataChunk(reader, (x) =>
                {
                    Palette = new Color[x.Elements];
                    for (int i = 0; i < Palette.Length; i++)
                    {
                        uint color = reader.ReadUInt32();
                        Palette[i].R = (byte)(color >> 16 & 0xFF);
                        Palette[i].G = (byte)(color >> 8 & 0xFF);
                        Palette[i].B = (byte)(color & 0xFF);
                    }
                });
            }

            // Read the image data chunk.
            ReadDataChunk(reader, (x) =>
            {
                Data = reader.ReadBytes(x.Elements * x.ElementSize);
            });
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the format in which pixel color data is stored.
        /// </summary>
        public PixFormat Format { get; private set; }

        /// <summary>
        /// Gets the dimensions (width and height) of the bitmap.
        /// </summary>
        public Vector2 Size { get; private set; }

        /// <summary>
        /// Gets or sets the name describing the bitmap.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the color palette referenced by pixel data.
        /// </summary>
        public Color[] Palette { get; set; }

        /// <summary>
        /// Gets or sets the raw format of the pixel data.
        /// </summary>
        public byte[] Data { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the <see cref="Color"/> of the pixel at the given coordinates.
        /// </summary>
        /// <param name="x">The X coordinate of the pixel.</param>
        /// <param name="y">The Y coordinate of the pixel.</param>
        /// <returns>The <see cref="Color"/> of the pixel.</returns>
        public Color GetColor(int x, int y)
        {
            int offset = y * Size.X + x;
            switch (Format)
            {
                case PixFormat.PAL8:
                    return Palette[Data[offset]];
                case PixFormat.RGB24:
                    offset *= 3;
                    return new Color(Data[offset], Data[offset + 1], Data[offset + 2]);
                case PixFormat.RGB555_BE:
                    {
                        offset *= 2;
                        ushort data = (ushort)(Data[offset] << 8 | Data[offset + 1]);
                        return new Color(
                            (byte)((data >> 10) << 3),
                            (byte)((data >> 5 & 0b11111) << 3),
                            (byte)((data & 0b11111) << 3));
                    }
                case PixFormat.RGB565_BE:
                    {
                        offset *= 2;
                        ushort data = (ushort)(Data[offset] << 8 | Data[offset + 1]);
                        return new Color(
                            (byte)((data >> 11) << 3),
                            (byte)((data >> 5 & 0b111111) << 2),
                            (byte)((data & 0b11111) << 3));
                    }
                default:
                    throw new InvalidOperationException("Unknown PIX data format.");
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void ReadHeaderChunk(BinaryDataReader reader, Action<PixHeader> callback)
        {
            PixChunkType chunkType = reader.ReadEnum<PixChunkType>(false);
            if (chunkType != PixChunkType.Header1 && chunkType != PixChunkType.Header2)
                throw new InvalidDataException("Missing expected PIX header chunk.");

            int length = reader.ReadInt32();
            PixFormat format = reader.ReadEnum<PixFormat>(true);
            ushort stride = reader.ReadUInt16();
            Vector2 size = new Vector2(reader.ReadUInt16(), reader.ReadUInt16());
            reader.Position += 6;
            string name = reader.ReadString(BinaryStringFormat.ZeroTerminated);

            callback.Invoke(new PixHeader()
            {
                Format = format,
                Stride = stride,
                Size = size,
                Name = name
            });
        }

        private void ReadDataChunk(BinaryDataReader reader, Action<PixData> callback)
        {
            PixChunkType chunkType = reader.ReadEnum<PixChunkType>(false);
            if (chunkType != PixChunkType.Data)
                throw new InvalidDataException("Missing expected PIX data chunk.");

            callback.Invoke(new PixData()
            {
                Length = reader.ReadInt32(),
                Elements = reader.ReadInt32(),
                ElementSize = reader.ReadInt32()
            });

            reader.Position += 8;
        }

        // ---- STRUCTURES ---------------------------------------------------------------------------------------------

        private struct PixHeader
        {
            internal PixFormat Format;
            internal ushort Stride;
            internal Vector2 Size;
            internal string Name;
        }

        private struct PixData
        {
            internal int Length;
            internal int Elements;
            internal int ElementSize;
        }
    }
}
