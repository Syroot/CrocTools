bl_info = {
    "name": "Croc MOD format",
    "description": "Import-Export BFRES mesh, UV's, materials and textures",
    "author": "Syroot",
    "version": (0, 1, 0),
    "blender": (2, 78, 0),
    "location": "File > Import-Export",
    "warning": "This add-on is under development.",
    "wiki_url": "https://github.com/Syroot/CrocTools/wiki",
    "tracker_url": "https://github.com/Syroot/CrocTools/issues",
    "support": 'COMMUNITY',
    "category": "Import-Export"
}

# Reload the package modules when reloading add-ons in Blender with F8.
if "bpy" in locals():
    import importlib
    if "addon" in locals():
        importlib.reload(addon)
    if "binaryio" in locals():
        importlib.reload(binaryio)
    if "mod" in locals():
        importlib.reload(mod)
import bpy
from . import addon
from . import binaryio
from . import mod


def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_import.append(addon.ImportOperator.menu_func_import)


def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_import.remove(addon.ImportOperator.menu_func_import)
