import bmesh
import bpy
import bpy_extras
from io_mesh_mod.binaryio import BinaryReader
from io_mesh_mod.mod import ModelFile, Mesh, Face


class ImportOperator(bpy.types.Operator, bpy_extras.io_utils.ImportHelper):
    """Load a Croc MOD file"""
    bl_idname = "import_mesh.mod"
    bl_label = "Import MOD"
    bl_options = {'UNDO'}
    filename_ext = ".mod"
    filter_glob = bpy.props.StringProperty(default="*.mod", options={'HIDDEN'})
    filepath = bpy.props.StringProperty(name="File Path", description="Filepath used for importing the MOD file", maxlen=1024)

    @staticmethod
    def menu_func_import(self, context):
        self.layout.operator(ImportOperator.bl_idname, text="Croc MOD (.mod)")

    def execute(self, context):
        model = None
        with open(self.properties.filepath, "rb") as raw:
            model = ModelFile()
            model.load(raw)
        # Create an object for the model file.
        model_ob = bpy.data.objects.new("model_ob", None)
        bpy.context.scene.objects.link(model_ob)
        # Append meshes for each model mesh.
        for model_mesh in model.meshes:
            bm = bmesh.new()
            # Add the vertices to the BMesh.
            for vertex in model_mesh.vertices:
                bm.verts.new((vertex[0], -vertex[2], vertex[1]))
            bm.verts.ensure_lookup_table()
            bm.verts.index_update()
            # Connect the vertices as defined by the faces.
            for model_face in model_mesh.faces:
                verts = bm.verts
                model_indices = model_face.indices
                face = bm.faces.new((verts[model_indices[2]], verts[model_indices[1]], verts[model_indices[0]]))
                if model_face.flags & 0b00001000:
                    # Quad face.
                    face = bm.faces.new((verts[model_indices[2]], verts[model_indices[3]], verts[model_indices[1]])) 
            # Create a mesh.
            mesh = bpy.data.meshes.new("mesh")
            bm.to_mesh(mesh)
            bm.free()
            mesh_ob = bpy.data.objects.new("mesh_ob", mesh)
            mesh_ob.parent = model_ob
            bpy.context.scene.objects.link(mesh_ob)
        return {'FINISHED'}