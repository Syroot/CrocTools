from io_mesh_mod.binaryio import BinaryReader


class ModelFile:
    def __init__(self):
        self.meshes = []
        self.flags = 0
        self.floor = 0
        self.ceiling = 0

    def load(self, raw):
        reader = BinaryReader(raw)
        mesh_count = reader.read_uint16()
        self.flags = reader.read_uint16()
        for i in range(mesh_count):
            mesh = Mesh()
            mesh._load(reader, self.flags)
            self.meshes.append(mesh)
        self.floor = reader.read_uint16()
        self.ceiling = reader.read_uint16()


class Mesh:
    def __init__(self):
        self.radius = 0
        self.bounding_box = []
        self.vertices = []
        self.unknownVertices = []
        self.faces = []
        self.unknowns = []

    def _load(self, reader: BinaryReader, flags):
        self.radius = reader.read_int32()
        # Read bounding box.
        for i in range(12):
            self.bounding_box.append((reader.read_int16(), reader.read_int16(), reader.read_int16())) 
        # Read vertices and unknown vertices.
        vertex_count = reader.read_int32()
        for i in range(vertex_count):
            self.vertices.append((reader.read_int16(), reader.read_int16(), reader.read_int16(), reader.read_int16()))
        for i in range(vertex_count):
            self.unknownVertices.append((reader.read_int16(), reader.read_int16(), reader.read_int16(), reader.read_int16()))
        # Read faces.
        face_count = reader.read_int32()
        for i in range(face_count):
            face = Face()
            face._load(reader)
            self.faces.append(face)
        # Read unkown optional structure.
        if flags & 1:
            unkCount1 = reader.read_int16()
            unkCount2 = reader.read_int16()
            for x in range(unkCount1 + unkCount2):
                unknown = Unknown()
                unknown._load(reader)
                self.unknowns.append(unknown)


class Face:
    def __init__(self):
        self.material_name = None
        self.normal = (0, 0, 0, 0)
        self.indices = []
        self.color = (0, 0, 0)
        self.flags = 0

    def _load(self, reader: BinaryReader):
        pos_after_name = reader.tell() + 64
        self.material_name = reader.read_string_0()
        reader.seek(pos_after_name)
        self.normal = (reader.read_int16(), reader.read_int16(), reader.read_int16(), reader.read_int16())
        self.indices = (reader.read_uint16(), reader.read_uint16(), reader.read_uint16(), reader.read_uint16())
        self.color = (reader.read_byte(), reader.read_byte(), reader.read_byte())
        self.flags = reader.read_byte()


class Unknown:
    def __init__(self):
        self.data = []

    def _load(self, reader: BinaryReader):
        self.data = reader.read_int32s(11)
